#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <time.h>
#include "dds/dds.h"
#include "MBCData.h"
#include "dds_create_entities.h"
#include "Sensors.h"
#include "dds_read_write_topic.h"

/* Define max number of MBCs. */
#define MAX_SEARCH 11 

volatile sig_atomic_t stop;

/* Function to handle keyboard interrupt (Ctrl + C). */
void inthand(int signum) {
  stop = 1;
}

int main (int argc, char *argv[])
{
  dds_entity_t participant;
  dds_entity_t topics[MAX_SEARCH];
  dds_entity_t readers[MAX_SEARCH];
  dds_return_t rc_read1;
  dds_return_t rc_read2; 
  dds_entity_t topic_write;
  dds_entity_t writer;
  dds_return_t rc_write;

  int MBC_id;
  int error_write;
  int error_read1;
  int error_read2;
  int i;
  int j;
  
  char array_convert[10];
  char topic_id[10]= "Topic";

  srand(time(0)); //Initialize random seed for traffic rng   
  signal(SIGINT, inthand); 

  if( argc == 2 ) {
    MBC_id = atoi(argv[1]);
    printf("Matrix board with ID number %d is online\n", MBC_id);
  }
  else if( argc > 2 ) {
    printf("Too many arguments supplied.\n");
    return EXIT_FAILURE;
  }
  else {
    printf("One argument expected.\n");
    return EXIT_FAILURE;
  } 

  /* Create a Participant.*/
  participant = dds_create_participant (DDS_DOMAIN_DEFAULT, NULL, NULL);
  if (participant < 0)
    DDS_FATAL("dds_create_participant: %s\n", dds_strretcode(-participant));

  /* Create MBC's own write Topic. */
  topic_write = create_topic (MBC_id, participant);
 
  /* Create MBC's own Writer. */
  writer = create_writer (participant, topic_write);
  
  /* Build Topic sender id. */
  sprintf(array_convert,"%d",MBC_id);
  strcat(topic_id," ");
  strcat(topic_id,array_convert);
  
  /* Create readers array on all topics. */
  for (i = MBC_id + 1; i < MAX_SEARCH; i++){
    topics[i] = create_topic (i, participant);
    readers[i] = create_reader (participant, topics[i]);
  }
 
  while (!stop)
  {
    error_write = write_topic(writer, rc_write, MBC_id, topic_id);
    for (i = MBC_id + 1; i < MAX_SEARCH-1; i++){
      error_read1 = read_topic(readers[i], rc_read1, MBC_id);
      if (error_read1){
        for (j = i+1;j < MAX_SEARCH-1; j++){
          error_read2 = read_topic(readers[j], rc_read2, MBC_id);
          if (error_read2) {
            break;
          }
        }
      }
      if (error_read1 && error_read2) {
        break;
      }
    }
    dds_sleepfor (DDS_MSECS (1000));
  }

  /* Deleting the participant will delete all its children recursively as well. */
  rc_read1 = dds_delete (participant);
  if (rc_read1 != DDS_RETCODE_OK)
     DDS_FATAL("dds_delete: %s\n", dds_strretcode(-rc_read1));
  
  printf("\nExiting safely >> Bye Bye from Matrix Board %d \n", MBC_id);
  return EXIT_SUCCESS;
}
