#include <stdio.h>
#include <stdlib.h> 
#include "Sensors.h"

/*Function creating random traffic in range (lower,upper). */
int create_traffic(){ 

  int upper = 50;
  int lower = 10;
  int num = (rand() % (upper - lower + 1)) + lower; 
  return num;
}

/*Function to adapt the displayed speed given the traffic received by that MBC. */
int adapt_speed(int32_t traffic){

  static int old_traffic;
  static int speed = MAX_SPEED; 
  if (old_traffic < traffic){
    if (speed > LOW_SPEED)
      speed -= 10;
  }
  if (old_traffic > traffic){
    if (speed < MAX_SPEED)
      speed += 10;
  }
  printf("Displayed Speed: %"PRId32"\n", speed);
  old_traffic = traffic;
  return traffic; 
}
