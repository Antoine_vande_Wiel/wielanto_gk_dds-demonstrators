#!/bin/bash

BASESEND="tcp://localhost:"
BASEREC="tcp://*:"

P1SEND=${BASESEND}5555
P1REC=${BASEREC}5556

P1SEND=${BASESEND}5556
P1REC=${BASEREC}5555

MASTER="1"
HUB="0"


trap ctrl_c INT

ctrl_c() {
		echo "knock knock, cleaning lady"
       	sh cleanup.sh
}

echo ${BASEREC} ${BASESEND} ${HUB}
python3 ZMQrrrisc.py ${BASEREC}5555 ${BASESEND}5556 ${HUB} &
sleep 1 
echo ${BASEREC} ${BASESEND}
python3 ZMQrrrisc.py ${BASEREC}5556 ${BASESEND}5555 ${MASTER}

cleanup.sh
