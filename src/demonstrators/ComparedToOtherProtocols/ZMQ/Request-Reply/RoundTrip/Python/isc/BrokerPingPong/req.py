#
#   Request-reply client in Python
#   Connects REQ socket to tcp://localhost:5559
#   Sends "Hello" to server, expects "World" back
#
import zmq
import time 
from datetime import datetime
import sys


RequestUrl=         sys.argv[1]         if len(sys.argv) > 1 else "tcp://localhost:5559"



#  Prepare our context and sockets
context = zmq.Context()
socket = context.socket(zmq.REQ)
socket.connect(RequestUrl)

AmountOfTrips = 10000
request = 0

StartTime = datetime.now()


#  Do 10 requests, waiting each time for a response
while request < AmountOfTrips:
    print("sending request to broker")
    socket.send(bytes(str(request), 'utf-8'))
    message = int(socket.recv().decode("utf-8"))
    print("Received reply %s [%s]" % (request, message))
    request = message + 1
    
    
EndTime = datetime.now()

print("It took %s seconds to send and receive %s messages" % ((EndTime-StartTime).total_seconds(),AmountOfTrips))