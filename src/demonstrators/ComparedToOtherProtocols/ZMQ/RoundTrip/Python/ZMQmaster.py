#!/usr/bin/env python3

import zmq

import sys
from time import sleep
from datetime import datetime

ReceievePort =  int(sys.argv[1]) if len(sys.argv) > 1 else 8881 # take the receive port from command line input or use the default one
SendPort =  int(sys.argv[2]) if len(sys.argv) > 2 else 8882
next_hub_IP = sys.argv[3] if len(sys.argv) > 3 else "localhost"


#ReceievePort = 8881
#SendPort = 8882

MAX=1000000
EVERY=5*1000

def report(c, t0, done=None):
    t1 = datetime.now()
    t = (t1-t0).total_seconds()
    print("Ping %s %i messages, in %2.1f seconds: %i msg/sec [%3.3f ms/msg]" % (
        (done if done else "already"), c, t, c/t, t*1000/c))

def main():

    context = zmq.Context()
    sender = context.socket(zmq.PAIR)
    print("tcp://"+next_hub_IP+":%s" % SendPort)
    sender.connect("tcp://"+next_hub_IP+":%s" % SendPort)
    
    context = zmq.Context()
    receiver = context.socket(zmq.PAIR)                              # XXX GAM: hardcoded, use also other network-patterns
    print("tcp://*:%s" % ReceievePort)
    receiver.bind("tcp://*:%s" % ReceievePort)
    
    count = 1
    sender.send_string("Echo %i" % count)
    
    t0 = datetime.now()
    
    for i in range(1,MAX+1):
        msg = receiver.recv().decode()
        count = int(msg.split(' ')[1])
        count +=1
        sender.send_string("Echo %i" % count)
        if i % EVERY == 0:
            report(i, t0)
        #sleep(1)
    pass



if __name__ == "__main__":
    main()