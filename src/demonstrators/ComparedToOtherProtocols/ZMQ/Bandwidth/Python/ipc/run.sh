#!/bin/bash
BASEPORT="888"
HUB="0"
MASTER="1"

# BASESEND="tcp://localhost:888"
# RECEIVEURL="tcp://*:888"

SOCKETLOCATION="ipc:///tmp/feeds"

mkdir /tmp/feeds

trap ctrl_c INT

ctrl_c() {
		echo "knock knock, cleaning lady"
       	sh cleanup.sh
}

python3 ZMQbipc.py ${SOCKETLOCATION}1 ${SOCKETLOCATION}2  0 &
sleep 1
python3 ZMQbipc.py ${SOCKETLOCATION}2 ${SOCKETLOCATION}3  0 &
sleep 1
python3 ZMQbipc.py ${SOCKETLOCATION}3 ${SOCKETLOCATION}4  0 &
sleep 1
python3 ZMQbipc.py ${SOCKETLOCATION}4 ${SOCKETLOCATION}1  1

sh cleanup.sh

les