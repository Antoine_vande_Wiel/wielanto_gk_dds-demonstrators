.. _Installation:

************
Installation
************

Message-passing systems use either distributed or local objects. With distributed objects the sender and receiver may be on different computers, running different operating systems, using different programming languages, etc. In this case the bus layer takes care of details about converting data from one system to another, sending and receiving data across the network, etc. The following types of protocols will be handled

.. toctree::
   :titlesonly:
   :glob:

   DDS
   MQTT
   ZMQ

