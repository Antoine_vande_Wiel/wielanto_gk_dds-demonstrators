ZMQ: ZeroMQ
===========



Running ZMQ
***********

before running anything the following ASSUMPTIONS are made here:

   * ZMQ is installed as per the IPSC manual
   * you have four raspberries configured as per the setup chapter
   * the repository has been downloaded

<ZMQ roundtrip explanation of how to use them>

For ZMQ the round trip is not as nicely worked out as for MQTT, however the bandwidth programs are easy to start::

   sh <techpush-sourcedir>/demonstrators/ComparedToOtherProtocols/ZMQ/Bandwidth/Python/isc/run.sh
   sh <techpush-sourcedir>/demonstrators/ComparedToOtherProtocols/ZMQ/Bandwidth/Python/ipc/run.sh






About ZMQ
*********

ZMQ is a very versatile library that makes implementing different ways of sending messages possible with one library and still tries to keep it simple. There are four main ways of communicationg messages:
   * Request-reply: client sends a request to a service and gets information back.
   * Publish-subscribe : client publishes message to several other clients whom are listening
   * Pipeline  : 
   * Exclusive pair
   
These methods of handling messages can be connected in the following ways:

   * PUB and SUB
   * REQ and REP
   * REQ and ROUTER (take care, REQ inserts an extra null frame)
   * DEALER and REP (take care, REP assumes a null frame)
   * DEALER and ROUTER
   * DEALER and DEALER
   * ROUTER and ROUTER
   * PUSH and PULL
   * PAIR and PAIR
   
