Tips and tricks
===============

Sphinx continuous build
-----------------------

Instead of writing in sphinx pressing build to see if it comes out correctly its much more efficient
to make a script that continuously builds so you just have too push refresh in your browser. 

in windows this can be done with: 

.. code-block:: python

   :loop
   
   DEL /q <buildDir>\* 
   
   FOR /L %%A IN (1,1,20) DO (
   sphinx-build -b html <techpushDir>\mess.techpush-training\docs\doc  <buildDir>
   )
   
   
   goto loop

This will keep building sphinx forever and once every 20 builds it does a clean build. 

Use shell scripts
-----------------

They might not be the most intuitive thing to work with but shell scripts are very usefull. 
Take for example the shell script used for the isc version of ZMQ bandwidth:: 

   BASEPORT="888"
   HUB="0"
   MASTER="1"
   
   BASESEND1="tcp://"
   BASESEND2=":8881"
   RECEIVEURL="tcp://*:8881"
   
   
   Pi0="192.168.0.200"
   Pi1="192.168.0.201"
   Pi2="192.168.0.202"
   Pi3="192.168.0.203"
   
   # copy MQTT.py the raspberries 
   scp ZMQbisc.py pi@${Pi0}:~/ZMQbisc.py
   scp ZMQbisc.py pi@${Pi1}:~/ZMQbisc.py
   scp ZMQbisc.py pi@${Pi3}:~/ZMQbisc.py
   
   ssh pi@${Pi0} 'mkdir /tmp/feeds'
   ssh pi@${Pi1} 'mkdir /tmp/feeds'
   ssh pi@${Pi3} 'mkdir /tmp/feeds'
   
   SOCKETLOCATION="ipc:///tmp/feeds"
   
   mkdir /tmp/feeds
   
   trap ctrl_c INT
   
   ctrl_c() {
         echo "knock knock, cleaning lady"
            sh cleanup.sh
   }
   
   ssh pi@${Pi0} python3 ~/ZMQbisc.py ${RECEIVEURL} ${BASESEND1}${Pi1}${BASESEND2}  0 &
   sleep 1
   ssh pi@${Pi1} python3 ~/ZMQbisc.py ${RECEIVEURL} ${BASESEND1}${Pi3}${BASESEND2}  0 &
   sleep 1
   ssh pi@${Pi3} python3 ~/ZMQbisc.py ${RECEIVEURL} ${BASESEND1}${Pi2}${BASESEND2}  0 &
   
   python3 ZMQbisc.py ${RECEIVEURL} ${BASESEND1}${Pi0}${BASESEND2} 1
   
   
   sh cleanup.sh
   
This script makes it easy to start the round trip  even though it has scripts running on four different raspberry pi's.
Also the cleanup script makes sure that everything the shell script makes is removed when it stops.
This makes sure that there is no python program running in the background messing things up. Doing this all manually might not seem like much
work but doing it a hundred times on one day is.

Conclusion: USE SHELL SCRIPTS